#!/usr/bin/env python3
"""
Generate a IIIF 2.0 collection from an Excel 2007+ (XSLX) file
"""
from openpyxl import load_workbook
import argparse
import json
import os.path
import urllib.parse


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source',
        help='Path to the XSLX file',
    )
    parser.add_argument(
        'output',
        type=argparse.FileType('w'),
        nargs='?',
        default='-',
        help='JSON output path',
    )
    return parser


def main(source, output, **kwargs):
    wb = load_workbook(source, read_only=True)
    rows = wb.active.rows
    # Skip header
    next(rows)

    filename = os.path.splitext(os.path.basename(source))[0]
    # Any JSON-LD ID must be a URL
    fake_id = urllib.parse.urlunsplit(('http', '127.0.0.1', filename, '', ''))

    collection = {
        "@context": "https://iiif.io/api/presentation/2/context.json",
        "@id": fake_id,
        "@type": "sc:Collection",
        "label": filename,
        "manifests": [
            {
                "@id": str(row[-1].value),
                "@type": "sc:Manifest",
                "label": ', '.join(str(cell.value) for cell in row[:-1]),
            }
            for row in rows
        ]
    }

    json.dump(collection, output, indent=4)


if __name__ == '__main__':
    kwargs = vars(get_parser().parse_args())
    main(**kwargs)
