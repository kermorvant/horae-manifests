# -*- coding: utf-8 -*-
import pandas as pd
import requests
import os
import unicodedata

df = pd.read_csv('500_MSS.csv',sep=';',encoding='latin1')

errors = []
for index, row in df.iterrows():
    outname = (row['Pays']+'-'+row['Ville']+'-'+row['Etablissement']+'-'+row['Cote']).replace(' ','_')+'.json'
    #outname = (row['Cote']).replace(' ','_')+'.json'

    url = row['Lien IIIF']
    print (url, outname)

    if os.path.exists(outname):
        continue


    try:
        response = requests.get(url)
    except:
        print ('ERROR',outname)
        errors.append(outname)
    with open(outname,"w") as outfile:
        outfile.write(response.text)

print('number of errors',len(errors))
print (errors)
